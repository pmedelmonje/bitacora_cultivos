## Bitácora simple de cultivos

Programado por Pedro Medel M. en Python 3.10, con una interfaz gráfica en TkInter, y utilizando una base de datos SQLite 3.

### Versión actual

El programa ofrece al usuario las siguientes funciones principales:

- Una grilla que enlista las plantas registradas por el usuario, con la siguiente información distribuida en columnas:
    - Etiqueta
    - Nombre
    - Fecha de Siembra
    - Fecha de Germinación
    - Cosecha más reciente
    - Total de cosechas


- Mostrar un marco con los siguientes botones:

  - Añadir
  - Eliminar
  - Registrar germinación
  - Registrar cosecha


  El botón **Añadir** abre una ventana adicional que solicita al usuario los siguientes datos (todos obligatorios):
  - **Etiqueta:**  El programa busca acostumbrar al usuario a etiquetar cada una de las plantas que siembra, de modo de ayudarle a llevar un seguimiento detallado. Esto puede hacerse, por ejemplo, colocando un palito de helado donde se escriba la etiqueta, que no es necesario que sea algo complejo. Puede ser una letra y un número. La idea es que en este programa se escriba la misma etiqueta que la que está escrita en donde se sembró la planta.

  - **Nombre:** Aquí va el nombre de la planta. Por ejemplo: Acelgas, Espinacas, etc.

  - **Fecha de siembra:** Idealmente ingresar en formato mm/dd/aaaa (mes/día/año). Por ejemplo, para un 3 de junio de 2022: *06/03/2022*. Versiones futuras de este programa podrían incluir funciones que trabajarán con ese formato de fecha.

El botón **Eliminar** abre un cuadro para confirmar si se eliminará el registro seleccionado en la grilla. Al aceptar, se eliminará la fila correspondiente de la base de datos y ya no se mostrará en la lista.

El botón **Germinación** abrirá un cuadro para ingresar la fecha en que la planta de la fila seleccionada germinó. Se anotará predeterminadamente la fecha actual, pero el usuario la podrá modificar. Posteriormente, se actualizará el registro en la base de datos y en la fila del listado.

El botón **Cosecha** abrirá un cuadro para ingresar la fecha en que se realiza una cosecha a la planta correspondiente. Se anotará predeterminadamente la fecha actual, pero el usuario la podrá modificar. Automáticamente tras guardar el registro, se sumará 1 al campo **Total de cosechas**.

---

### Próximas modificaciones

- 2 o más plantas no deberían tener la misma etiqueta.
- Asegurar de que el usuario ingrese una fecha en formato mm/dd/aaaa.
- Añadir instrucciones de llenado en las ventanas con formularios. Que no solamente haya entradas de texto.
- (Tal vez) cambiar colores.
