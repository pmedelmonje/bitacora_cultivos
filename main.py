"""
Bitácora de cultivos, versión sencilla

Esta primera versión tendrá las siguientes columnas:

- Etiqueta
- Planta
- Siembra
- Germinación
- Cosecha más reciente
- Cantidad de cosechas
"""

import os, subprocess, platform

from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from datetime import datetime

from db import *

def showPlants():
    treeview.delete(*treeview.get_children())
    for row in fetchPlants():
        treeview.insert('', END, row[0], values=(row[1], row[2], row[3],
                        row[4], row[5], row[6]))


def addPlant():
    ''' Agregar nueva planta a la bitácora '''

    def saveAdd():
        if not eLabel.get() or not eName.get() or not eSow.get():
            errorLabel.grid(row=4, column = 0, columnspan = 2)
            return

        else:
            newPlant = {
            "label": eLabel.get(),
            "name": eName.get(),
            "sow": eSow.get(),
            "germination": " ",
            "harvest": " ",
            "total_harvests": 0
            }

            insertPlant(newPlant)
            showPlants()
            top.destroy()

    def cancelAdd():
        showPlants()
        top.destroy()

    top = Toplevel()
    top.title("Agregar nueva planta")
    top.configure(bg=darkGreen)

    topFrame = Frame(top, bg = lightGreen)
    topFrame.grid(row = 0, column = 0, padx=10, pady=10)

    lLabel = Label(topFrame, text = "Ingrese etiqueta ",bg=lightGreen)
    eLabel = Entry(topFrame, width = 20)
    lLabel.grid(row=0, column=0, sticky = "w", padx = 5)
    eLabel.grid(row=0, column=1, sticky = "w")

    lName = Label(topFrame, text = "Ingrese nombre ", bg=lightGreen)
    eName = Entry(topFrame, width = 20)
    lName.grid(row=1, column=0, sticky = "w", padx = 5)
    eName.grid(row=1, column=1, sticky = "w")

    lSow = Label(topFrame, text = "Ingrese fecha de siembra ", bg=lightGreen)
    eSow = Entry(topFrame, width=20)
    eSow.insert(0, today())
    lSow.grid(row=2, column=0, sticky = "w", padx = 5)
    eSow.grid(row=2,column=1, sticky = "w")

    btnSave = Button(topFrame, text= "Guardar", bg = "#5e8806", fg = "white",
                    command = saveAdd)
    btnSave.grid(row= 3, column = 0, padx=7, pady=7)

    btnCancel = Button(topFrame, text = "Cancelar", bg= "#8b7d03", fg = "white",
                        command = cancelAdd)
    btnCancel.grid(row = 3, column = 1)

    errorLabel = Label(topFrame, text = "¡Todos los valores son obligatorios!",
    bg = lightGreen)

    top.mainloop()

def reportGermination():
    ''' Reportar la germinación de la planta seleccionada '''

    def saveGermination():
        germinationDate = eGermDate.get()
        if not germinationDate:
            errorLabel.grid(row=1, column = 0, columnspan = 3)
            return

        else:
            updateGerm(id, germinationDate)
            showPlants()
            top.destroy()

    id = treeview.selection()[0]
    plant = fetchPlant(id)

    top = Toplevel()
    top.title("Reportar Germinación")
    top.configure(bg=lightGreen)

    lGermDate = Label(top, text="Ingrese fecha de germinación", bg = lightGreen)
    eGermDate = Entry(top, width=20)
    btnSaveGerm = Button(top, text="Guardar", bg="#0c8270", fg = "white",
                        command = saveGermination)
    lGermDate.grid(row = 0, column = 0, padx = 5, sticky = "w")
    eGermDate.insert(0, today())
    eGermDate.grid(row = 0, column = 1, sticky = "w")
    btnSaveGerm.grid(row = 0, column = 2, sticky = "w", padx=5, pady=3)

    errorLabel = Label(top, text = "¡No se debe guardar vacía!",
    bg = lightGreen)

    top.mainloop()


def reportHarvest():
    '''Reportar la cosecha más reciente de la planta seleccionada'''

    def saveHarvest():
        harvestDate = eHarvestDate.get()
        if not harvestDate:
            errorLabel.grid(row=1, column = 0, columnspan = 3)
            return

        else:
            updateHarvest(harvestDate, id)
            showPlants()
            top.destroy()

    id = treeview.selection()[0]
    plant = fetchPlant(id)

    # El programa no permite registrar una cosecha si no se ha registrado antes
    # la germinación de la respectiva planta

    if plant[4] == " ":
        messagebox.showwarning("Alerta", "Reporte la fecha de germinación antes de reportar cosecha")
        pass

    else:
        top = Toplevel()
        top.title("Reportar Cosecha")
        top.configure(bg=lightGreen)

        lHarvestDate = Label(top, text="Ingrese fecha de cosecha", bg = lightGreen)
        eHarvestDate = Entry(top, width=20)
        btnSaveHarvest = Button(top, text="Guardar", bg="#0c8270", fg = "white",
                            command = saveHarvest)
        lHarvestDate.grid(row = 0, column = 0, padx = 5, sticky = "w")
        eHarvestDate.insert(0, today())
        eHarvestDate.grid(row = 0, column = 1, sticky = "w")
        btnSaveHarvest.grid(row = 0, column = 2, sticky = "w", padx=5, pady=3)

        errorLabel = Label(top, text = "¡No se debe guardar vacía!",
        bg = lightGreen)

        top.mainloop()

def delPlant():
    '''Eliminar la planta seleccionada de la lista'''

    id = treeview.selection()[0]
    plant = fetchPlant(id)
    answer = messagebox.askokcancel('Advertencia', '¿Realmente quieres eliminar el registro '
    + plant[2] + '?')

    if answer:
        deleteRow(id)
        showPlants()
    else:
        pass


def today():
    '''Obtiene la fecha actual'''

    today = datetime.today().strftime("%m/%d/%Y")
    return today

def showHelp():
    if platform.system() == 'Darwin':       # macOS
        subprocess.call(('open', 'help.txt'))
    elif platform.system() == 'Windows':    # Windows
        os.startfile('help.txt')
    else:                                   # linux variants
        subprocess.call(('xdg-open', 'help.txt'))



def showAboutWindow():
    topAbout = Toplevel()
    topAbout.title("Acerca de")
    topAbout.configure(bg=darkGreen)
    #topAbout.minsize(width = 480, height = 320)
    topAbout.resizable(width = 0, height = 0)
    topAbout.grid_rowconfigure(0, weight = 1)
    topAbout.grid_columnconfigure(0, weight = 1)
    topAbout.grid_rowconfigure(1, weight = 1)
    topAbout.grid_columnconfigure(1, weight = 1)

    aboutFrame = Frame(topAbout, bg = lightGreen)
    aboutFrame.grid(row=0, column=0, padx=7, pady=7)

    aboutText = """
Bitácora de cultivos
Pedro Medel M. - 2022

Programa gratuito y de código abierto.
"""

    aboutLabel = Label(aboutFrame, text = aboutText,
                    bg = lightGreen)
    aboutLabel.grid(row = 0, column = 0, padx = 5, pady = 5)

    topAbout.mainloop()



# Configuración general

createDB()

lightGreen = "#A4C9BB"
darkGreen = "#007863"
grey = "#E6E6E6"
yuma = "#C8B88A"

root = Tk()
root.title("Bitácora de cultivos")
root.configure(bg= darkGreen)
root.minsize(width=720, height=400)
root.resizable(width = 0, height = 0)

# Marco principal

mainFrame = Frame(root, bg =lightGreen)
mainFrame.grid(row = 0, column = 0, padx = 10, pady = 10)

# Marco contenedor del Treeview

treeFrame = Frame(mainFrame, width = 400, height = 240, bg = grey)
treeFrame.grid(row = 0, column = 0, padx = 10, pady = 10, sticky="nwe")
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0, weight=1)
root.grid_rowconfigure(1, weight=1)
root.grid_columnconfigure(1, weight=1)
root.grid_rowconfigure(2, weight=1)
root.grid_columnconfigure(2, weight=1)

treeview = ttk.Treeview(treeFrame)
treeview['columns'] = ('Etiqueta','Planta','Siembra', 'Germinación',
                        'Ult. Cosecha', 'Cant. Cosechas')

treeview.column('#0', width = 0, stretch=NO)
treeview.column('Etiqueta', width = 70)
treeview.column('Planta', width = 150)
treeview.column('Siembra', width = 105)
treeview.column('Germinación', width = 105)
treeview.column('Ult. Cosecha', width = 105)
treeview.column('Cant. Cosechas', width = 110)

treeview.heading('#0', text='id')
treeview.heading('Etiqueta', text='Etiqueta')
treeview.heading('Planta', text='Planta')
treeview.heading('Siembra', text='Siembra')
treeview.heading('Germinación', text='Germinación')
treeview.heading('Ult. Cosecha', text='Ult. Cosecha')
treeview.heading('Cant. Cosechas', text='Cant. Cosechas')

treeview.grid(row=0, column=0, padx=15, pady=15)


# Marco contenedor de los Botones

btnFrame = LabelFrame(mainFrame, text = "Registros", width = 400, height = 240,
                        bg = lightGreen)
btnFrame.grid(row = 1, column = 0, padx = 10, pady = 10, sticky = "nwe")

btnAdd = Button(btnFrame, text="Agregar", bg="#5e8806", fg="white",
                command = addPlant)
btnAdd.grid(row=0, column=0, padx=7, pady=7)

btnDelete = Button(btnFrame, text="Eliminar", bg="#8b7d03", fg="white",
                command = delPlant)
btnDelete.grid(row = 0, column= 1, padx=7, pady=7)

btnGerm = Button(btnFrame, text="Germinación", bg = "#0c8270", fg="white",
                command = reportGermination)
btnGerm.grid(row=0, column=2, padx=7, pady=7)

btnHarvest = Button(btnFrame, text="Cosecha", bg="#0c8270", fg="white",
                command = reportHarvest)
btnHarvest.grid(row=0, column=3, padx=7, pady=7)

btnHelp = Button(btnFrame, text = "Ayuda", bg = yuma,
                command = showHelp)
btnHelp.grid(row = 0, column = 4, padx= 7, pady = 7)

btnAbout = Button(btnFrame, text = "Acerca de", bg = yuma,
                command = showAboutWindow)
btnAbout.grid(row = 0, column = 5, padx= 7, pady = 7)

showPlants()

root.mainloop()
