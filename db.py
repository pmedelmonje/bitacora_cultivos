import sqlite3
import os

dbName = "db1.db"

def createDB():
    if not os.path.exists(dbName):
        conn = sqlite3.connect(dbName)
        cursor = conn.cursor()
        cursor.execute(
        """CREATE TABLE IF NOT EXISTS plantas (
        id INTEGER PRIMARY KEY NOT NULL,
        etiqueta TEXT NOT NULL,
        nombre TEXT NOT NULL,
        siembra TIMESTAMP NOT NULL,
        germinacion TIMESTAMP,
        ultima_cosecha TIMESTAMP,
        cant_cosechas INTEGER
        );""")
        conn.commit()
        conn.close()

def insertPlant(newPlant):
    conn = sqlite3.connect(dbName)
    cursor = conn.cursor()
    cursor.execute(
    """insert into plantas(id, etiqueta, nombre, siembra, germinacion,
    ultima_cosecha, cant_cosechas) values (null, ?, ?, ?, ?, ?, ?);""",
    (newPlant["label"], newPlant["name"], newPlant["sow"], newPlant["germination"],
    newPlant["harvest"], newPlant["total_harvests"])
    )
    conn.commit()
    conn.close()

def updateGerm(id, germDate):
    conn = sqlite3.connect(dbName)
    cursor = conn.cursor()
    cursor.execute(f'UPDATE plantas SET germinacion = "{germDate}" WHERE id = {id};')
    conn.commit()
    conn.close()

def updateHarvest(harvestDate, id):
    conn = sqlite3.connect(dbName)
    cursor = conn.cursor()
    instructions = f'''UPDATE plantas SET ultima_cosecha = "{harvestDate}" WHERE id = {id};
                    UPDATE plantas SET cant_cosechas = cant_cosechas + 1 WHERE id = {id};'''
    cursor.executescript(instructions)
    conn.commit()
    conn.close()

def fetchPlants():
    conn = sqlite3.connect(dbName)
    cursor = conn.cursor()
    rows = cursor.execute("select * from plantas;").fetchall()
    return rows

def fetchPlant(id):
    conn = sqlite3.connect(dbName)
    cursor = conn.cursor()
    row = cursor.execute("select * from plantas where id = ?", (id, )).fetchone()
    return row

def deleteRow(id):
    conn = sqlite3.connect(dbName)
    cursor = conn.cursor()
    cursor.execute('DELETE FROM plantas WHERE id= ?', (id, ))
    conn.commit()
    conn.close()

if __name__ == "__main__":
    pass
