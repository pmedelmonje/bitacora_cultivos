==============
TEMAS DE AYUDA
==============

¿Cómo registrar una planta en el programa?

Tan fácil como pulsando el botón "Añadir". Inmediatamente se abrirá una ventana donde se solicitan los siguientes datos:

Etiqueta: Lo ideal es etiquetar las plantas se que se han sembrado, especialmente cuando se es principiante. Las ventajas de hacer esto son:

- Se sabe dónde fue sembrada la planta.
- Se puede recordar con facilidad qué se sembró.
- Facilita el seguimiento de la planta.

Las etiquetas no necesariamente tienen que ser algo complejo. Puede ser una letra y un número. Así de sencillo.

Nombre: Aquí va el nombre de la planta. Ejemplo: Acelgas, espinacas, etc.

Fechas: Se le solicita que las fechas (siembra, germinación y cosecha) se ingresen en formato mm/dd/aaaa (mes/día/año). Por ejemplo, para un 12 de enero de 2022: 01/12/2022. Cuando vaya a registrar una fecha, el programa escribe la fecha actual, y lo hace en el formato antes señalado. No obstante, usted puede cambiar la fecha si lo desea, procurando mantener el mismo formato.

Importante: Lo ideal es ingresar las fechas correspondientes a cada etapa de la planta lo antes posible, para poder llevar un seguimiento más preciso. El programa, además, va a permitir registrar una cosecha de una planta solamente si se ha registrado antes una germinación. 

---

¿Pueden tener varias plantas la misma etiqueta?

La idea es que no, para no confundirse. Si se ha sembrado una hilera de semillas de la cual se espera que germinen varias plantas, se recomienda registrar en el programa la cantidad de plantas que se espera que germinen. Si germinan más de las esperadas, se ingresan registros nuevos en el programa. Aproveche que no tiene límite de plantas a registrar. Si germinan menos de las plantas esperadas, simplemente borre los registros sobrantes.

Por ejemplo, las espinacas se suelen sembrar a 30cm de distancia. Si se siembra una hilera de semillas en un espacio de 1m de largo, se esperaría tener 3 o 4 plantas. Aún en esos casos, se aconseja usar etiquetas distintas, colocándolas a 30cm cada una, y registrándolas luego en el programa. Si se registró plantas de más, simplemente se borran después las que sobren.

Estoy pensando en añadir una sección en el programa donde se registre grupos de plantas, pero si lo hago, será más adelante.

---

¿Se puede recuperar la información de una planta que borré del programa?

Por el momento no. Se aconseja eliminar un registro solamente si está seguro(a).

---

¿Puedo exportar la información en un archivo, o compartirla?

Son ideas para una futura actualización.
